var Pattern = /** @class */ (function () {
    function Pattern() {
    }
    Pattern.prototype.menu = function (y) {
        var x = y;
        switch (x) {
            case 1:
                this.pattern1();
                break;
            case 2:
                this.pattern2();
                break;
            case 3:
                this.pattern3();
                break;
            case 4:
                this.pattern4();
                break;
            case 5:
                this.pattern5();
                break;
            case 6:
                this.pattern6();
                break;
            case 7:
                this.pattern7();
                break;
            case 8:
                this.pattern8();
                break;
            case 9:
                this.pattern9();
                break;
            case 10:
                this.pattern10();
                break;
            case 11:
                this.pattern11();
                break;
            case 12:
                this.pattern12();
                break;
            case 13:
                this.pattern13();
                break;
            case 14:
                this.pattern14();
                break;
            case 15:
                this.pattern15();
                break;
            case 16:
                this.pattern16();
                break;
            case 17:
                this.pattern17();
                break;
            case 18:
                this.pattern18();
                break;
            case 19:
                this.pattern19();
                break;
            case 20:
                this.pattern20();
                break;
            case 21:
                this.pattern21();
                break;
            case 22:
                this.pattern22();
                break;
            case 23:
                this.pattern23();
                break;
            case 24:
                this.pattern24();
                break;
            case 25:
                this.pattern25();
                break;
            case 26:
                this.pattern26();
                break;
            case 27:
                this.pattern27();
                break;
            case 28:
                this.pattern28();
                break;
            case 29:
                this.pattern29();
                break;
            case 30:
                this.pattern30();
                break;
            case 31:
                this.pattern31();
                break;
            default:
                console.log("Pilihan tidak tersedia");
                break;
        }
    };
    Pattern.prototype.pattern1 = function () {
        var size = 5;
        var jmlStar = 0;
        var output = '';
        for (var i = 0; i <= size; i++) {
            output = '';
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            jmlStar++;
            console.log(output);
        }
    };
    Pattern.prototype.pattern2 = function () {
        var size = 5;
        var jmlStar = 0;
        var fronSpace = size;
        var output = '';
        for (var i = 0; i <= size; i++) {
            output = '';
            for (var j = 0; j < fronSpace; j++) {
                output = output + ' ';
            }
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            jmlStar++;
            fronSpace--;
            console.log(output);
        }
    };
    Pattern.prototype.pattern3 = function () {
        var size = 5;
        var jmlStar = size;
        var output = '';
        for (var i = 0; i <= size; i++) {
            output = '';
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            jmlStar--;
            console.log(output);
        }
    };
    Pattern.prototype.pattern4 = function () {
        var size = 5;
        var jmlStar = size;
        var fronSpace = 0;
        var output = '';
        for (var i = 0; i <= size; i++) {
            output = '';
            for (var j = 0; j < fronSpace; j++) {
                output = output + ' ';
            }
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            jmlStar--;
            fronSpace++;
            console.log(output);
        }
    };
    Pattern.prototype.pattern5 = function () {
        var size = 5;
        var jmlStar = 0;
        var fronSpace = size;
        var output = '';
        for (var i = 0; i < size; i++) {
            output = '';
            for (var j = 0; j < fronSpace; j++) {
                output = output + ' ';
            }
            for (var j = 0; j <= jmlStar; j++) {
                output = output + '*';
            }
            jmlStar += 2;
            fronSpace--;
            console.log(output);
        }
    };
    Pattern.prototype.pattern6 = function () {
        var size = 5;
        var jmlStar = (size * 2) - 1;
        var fronSpace = 0;
        var output = '';
        for (var i = 0; i <= size; i++) {
            output = '';
            for (var j = 0; j < fronSpace; j++) {
                output = output + ' ';
            }
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            jmlStar -= 2;
            fronSpace++;
            console.log(output);
        }
    };
    Pattern.prototype.pattern7 = function () {
        var size = 9;
        var jmlStar = 0;
        var fronSpace = size / 2;
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar += 2;
                fronSpace--;
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar -= 2;
                fronSpace++;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern8 = function () {
        var size = 9;
        var jmlStar = 0;
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar++;
            }
            else {
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar--;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern9 = function () {
        var size = 9;
        var jmlStar = 0;
        var output = '';
        var fronSpace = size / 2;
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar++;
                fronSpace--;
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar--;
                fronSpace++;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern10 = function () {
        var size = 5;
        var jmlStar = size;
        var output = '';
        var fronSpace = size;
        for (var i = 0; i < size; i++) {
            output = '';
            for (var j = 0; j < fronSpace; j++) {
                output = output + ' ';
            }
            for (var j = 0; j <= jmlStar; j++) {
                output = output + '*';
            }
            fronSpace--;
            console.log(output);
        }
    };
    Pattern.prototype.pattern11 = function () {
        var size = 5;
        var jmlStar = size;
        var output = '';
        var fronSpace = 0;
        for (var i = 0; i < size; i++) {
            output = '';
            for (var j = 0; j < fronSpace; j++) {
                output = output + ' ';
            }
            for (var j = 0; j <= jmlStar; j++) {
                output = output + '*';
            }
            fronSpace++;
            console.log(output);
        }
    };
    Pattern.prototype.pattern12 = function () {
        var size = 9;
        var jmlStar = (size / 2);
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i <= size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar--;
            }
            else {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar++;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern13 = function () {
        var size = 9;
        var jmlStar = (size / 2);
        var frontSpace = 0;
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j < frontSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar--;
                frontSpace++;
            }
            else {
                for (var j = 0; j < frontSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar++;
                frontSpace--;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern14 = function () {
        var size = 9;
        var jmlStar = size / 2;
        var fronSpace = 0;
        var c = jmlStar - 1;
        var output = '';
        for (var i = 0; i < size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '* ';
                }
                jmlStar--;
                fronSpace++;
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '* ';
                }
                jmlStar++;
                fronSpace--;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern15 = function () {
        var size = 5;
        var jmlStar = 0;
        var output = '';
        for (var i = 0; i <= size; i++) {
            output = '';
            if (i == size) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
            }
            jmlStar++;
            console.log(output);
        }
    };
    Pattern.prototype.pattern16 = function () {
        var size = 5;
        var jmlStar = 0;
        var fronSpace = size;
        var output = '';
        for (var i = 0; i <= size; i++) {
            output = '';
            if (i == size) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
            }
            jmlStar++;
            fronSpace--;
            console.log(output);
        }
    };
    Pattern.prototype.pattern17 = function () {
        var size = 5;
        var jmlStar = size;
        var output = '';
        for (var i = 0; i <= size; i++) {
            output = '';
            if (i == 0) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
            }
            jmlStar--;
            console.log(output);
        }
    };
    Pattern.prototype.pattern18 = function () {
        var size = 5;
        var jmlStar = size;
        var frontSpace = 0;
        var output = '';
        for (var i = 0; i <= size; i++) {
            output = '';
            if (i == 0) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < frontSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
            }
            jmlStar--;
            frontSpace++;
            console.log(output);
        }
    };
    Pattern.prototype.pattern19 = function () {
        var size = 5;
        var jmlStar = 0;
        var fronSpace = size - 1;
        var output = '';
        for (var i = 0; i < size; i++) {
            output = '';
            if (i == size - 1) {
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
            }
            jmlStar += 2;
            fronSpace--;
            console.log(output);
        }
    };
    Pattern.prototype.pattern20 = function () {
        var size = 5;
        var jmlStar = (size * 2) - 1;
        var fronSpace = 0;
        var output = '';
        for (var i = 0; i < size; i++) {
            output = '';
            if (i == 0) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
            }
            jmlStar -= 2;
            fronSpace++;
            console.log(output);
        }
    };
    Pattern.prototype.pattern21 = function () {
        var size = 9;
        var jmlStar = 0;
        var fronSpace = (size / 2) - 1;
        var output = '';
        var c = fronSpace;
        for (var i = 0; i < size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
                jmlStar += 2;
                fronSpace--;
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
                jmlStar -= 2;
                fronSpace++;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern22 = function () {
        var size = 9;
        var jmlStar = 0;
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
                jmlStar++;
            }
            else {
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
                jmlStar--;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern23 = function () {
        var size = 9;
        var jmlStar = 0;
        var output = '';
        var frontSpace = (size / 2) - 1;
        var c = frontSpace;
        for (var i = 0; i < size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j <= frontSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
                frontSpace--;
                jmlStar++;
            }
            else {
                for (var j = 0; j <= frontSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
                jmlStar--;
                frontSpace++;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern24 = function () {
        var size = 5;
        var jmlStar = size;
        var output = '';
        var fronSpace = size;
        for (var i = 0; i < size; i++) {
            output = '';
            if (i == 0 || i == size - 1) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
            }
            fronSpace--;
            console.log(output);
        }
    };
    Pattern.prototype.pattern25 = function () {
        var size = 5;
        var jmlStar = size;
        var output = '';
        var fronSpace = 0;
        for (var i = 0; i < size; i++) {
            output = '';
            if (i == 0 || i == size - 1) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
            }
            fronSpace++;
            console.log(output);
        }
    };
    Pattern.prototype.pattern26 = function () {
        var size = 9;
        var jmlStar = size;
        var star = 0;
        var c = (size / 2) - 1;
        var output = '';
        for (var i = 0; i < size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j < star; j++) {
                    output = output + '*';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
                for (var j = 0; j < star; j++) {
                    output = output + '*';
                }
                jmlStar -= 2;
                star++;
            }
            else {
                for (var j = 0; j < star; j++) {
                    output = output + '*';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
                for (var j = 0; j < star; j++) {
                    output = output + '*';
                }
                jmlStar += 2;
                star--;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern27 = function () {
        var size = 10;
        var jmlStar = 0;
        var fronSpace = size / 2;
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
                jmlStar += 2;
                fronSpace--;
            }
            else if (i == c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
                jmlStar -= 2;
                fronSpace++;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern28 = function () {
        var size = 9;
        var jmlStar = 0;
        var fronSpace = (size / 2) - 1;
        var output = '';
        var c = fronSpace;
        for (var i = 0; i < size; i++) {
            output = '';
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
                jmlStar += 2;
                fronSpace--;
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + ' ';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
                jmlStar -= 2;
                fronSpace++;
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern29 = function () {
        var size = 9;
        var jmlStar = 9;
        var fronSpace = 1;
        var c = (jmlStar / 2) - 1;
        var output = '';
        for (var i = 0; i < size; i++) {
            output = '';
            if (i == 0 || i == size - 1) {
                jmlStar = 9;
                for (var j = 0; j <= (jmlStar / 2); j++) {
                    output = output + '* ';
                }
            }
            else {
                if (i < c) {
                    for (var j = 0; j < fronSpace; j++) {
                        output = output + ' ';
                    }
                    for (var j = 0; j < jmlStar - 2; j++) {
                        if (j == 0 || j == jmlStar - 3) {
                            output = output + '*';
                        }
                        else {
                            output = output + ' ';
                        }
                    }
                    jmlStar--;
                    jmlStar--;
                    fronSpace++;
                }
                else if (i == c) {
                    for (var j = 0; j < jmlStar - 2; j++) {
                        if (j == 0 || j == jmlStar - 3) {
                            output = output + '*';
                        }
                        else {
                            output = output + ' ';
                        }
                    }
                }
                else {
                    for (var j = 0; j < fronSpace; j++) {
                        output = output + ' ';
                    }
                    for (var j = 0; j < jmlStar - 2; j++) {
                        if (j == 0 || j == jmlStar - 3) {
                            output = output + '*';
                        }
                        else {
                            output = output + ' ';
                        }
                    }
                    jmlStar++;
                    jmlStar++;
                    fronSpace--;
                }
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern30 = function () {
        var size = 5;
        var jmlStar = size;
        var output = '';
        for (var i = 0; i < size; i++) {
            output = '';
            for (var j = 0; j <= jmlStar; j++) {
                output = output + '*';
            }
            console.log(output);
        }
    };
    Pattern.prototype.pattern31 = function () {
        var size = 5;
        var jmlStar = size;
        var output = '';
        for (var i = 0; i < size; i++) {
            output = '';
            if (i == 0 || i == size - 1) {
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + ' ';
                    }
                }
            }
            console.log(output);
        }
    };
    return Pattern;
}());
// const pattern = new Pattern();
// pattern.pattern27();
var pattern = new Pattern();
pattern.menu(1);

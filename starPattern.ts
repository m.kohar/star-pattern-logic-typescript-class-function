class Pattern {
   
   menu(y: number) {
      let x: number = y;

      switch (x) {
         case 1:
            this.pattern1();
            break;
         case 2:
            this.pattern2();
            break;
         case 3:
            this.pattern3();
            break;
         case 4:
            this.pattern4();
            break;
         case 5:
            this.pattern5();
            break;
         case 6:
            this.pattern6();
            break;
         case 7:
            this.pattern7();
            break;
         case 8:
            this.pattern8();
            break;
         case 9:
            this.pattern9();
            break;
         case 10:
            this.pattern10();
            break;
         case 11:
            this.pattern11();
            break;
         case 12:
            this.pattern12();
            break;
         case 13:
            this.pattern13();
            break;
         case 14:
            this.pattern14();
            break;
         case 15:
            this.pattern15();
            break;
         case 16:
            this.pattern16();
            break;
         case 17:
            this.pattern17();
            break;
         case 18:
            this.pattern18();
            break;
         case 19:
            this.pattern19();
            break;
         case 20:
            this.pattern20();
            break;
         case 21:
            this.pattern21();
            break;
         case 22:
            this.pattern22();
            break;
         case 23:
            this.pattern23();
            break;
         case 24:
            this.pattern24();
            break;
         case 25:
            this.pattern25();
            break;
         case 26:
            this.pattern26();
            break;
         case 27:
            this.pattern27();
            break;
         case 28:
            this.pattern28();
            break;
         case 29:
            this.pattern29();
            break;
         case 30:
            this.pattern30();
            break;
         case 31:
            this.pattern31();
            break;
         default:
            console.log("Pilihan tidak tersedia");
            break;
     }

   }

   pattern1() {
      var size: number = 5;
      var jmlStar: number = 0;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         output = '';
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         jmlStar++;
         console.log(output);
      }
   }

   pattern2() {
      var size: number = 5;
      var jmlStar: number = 0;
      var fronSpace: number = size;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         output = '';
         for (let j = 0; j < fronSpace; j++) {
            output = output + ' ';
         }
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         jmlStar++;
         fronSpace--;
         console.log(output);
      }
   }

   pattern3() {
      var size: number = 5;
      var jmlStar: number = size;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         output = '';
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         jmlStar--;
         console.log(output);
      }
   }

   pattern4() {
      var size: number = 5;
      var jmlStar: number = size;
      var fronSpace: number = 0;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         output = '';
         for (let j = 0; j < fronSpace; j++) {
            output = output + ' ';
         }
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         jmlStar--
         fronSpace++;
         console.log(output);
      }
   }

   pattern5() {
      var size: number = 5;
      var jmlStar: number = 0;
      var fronSpace: number = size;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         output = '';
         for (let j = 0; j < fronSpace; j++) {
            output = output + ' ';
         }
         for (let j = 0; j <= jmlStar; j++) {
            output = output + '*';
         }
         jmlStar += 2;
         fronSpace--;
         console.log(output);
      }
   }

   pattern6() {
      var size: number = 5;
      var jmlStar: number = (size * 2) - 1;
      var fronSpace: number = 0;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         output = '';
         for (let j = 0; j < fronSpace; j++) {
            output = output + ' ';
         }
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         jmlStar -= 2;
         fronSpace++;
         console.log(output);
      }
   }

   pattern7() {
      var size: number = 9;
      var jmlStar: number = 0;
      var fronSpace: number = size / 2;
      var output: string = '';
      let c: number = (size / 2) - 1;

      for (let i = 0; i < size; i++) {
         output = '';

         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar += 2;
            fronSpace--;
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar -= 2;
            fronSpace++;
         }
         
         console.log(output);
      }
   }

   pattern8() {
      var size: number = 9;
      var jmlStar: number = 0;
      var output: string = '';
      let c: number = (size / 2) - 1;

      for (let i = 0; i < size; i++) {
         output = '';

         if (i < c) {
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar++;
         } else {
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar--;
         }
         
         console.log(output);
      }
   }

   pattern9() {
      var size: number = 9;
      var jmlStar: number = 0;
      var output: string = '';
      var fronSpace: number = size / 2;
      let c: number = (size / 2) - 1;

      for (let i = 0; i < size; i++) {
         output = '';

         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar++;
            fronSpace--;
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar--;
            fronSpace++;
         }
         
         console.log(output);
      }
   }

   pattern10() {
      var size: number = 5;
      var jmlStar: number = size;
      var output: string = '';
      var fronSpace: number = size;

      for (let i = 0; i < size; i++) {
         output = '';

         for (let j = 0; j < fronSpace; j++) {
            output = output + ' ';
         }
         for (let j = 0; j <= jmlStar; j++) {
            output = output + '*';
         }
         fronSpace--;
         
         console.log(output);
      }
   }
   
   pattern11() {
      var size: number = 5;
      var jmlStar: number = size;
      var output: string = '';
      var fronSpace: number = 0;

      for (let i = 0; i < size; i++) {
         output = '';

         for (let j = 0; j < fronSpace; j++) {
            output = output + ' ';
         }
         for (let j = 0; j <= jmlStar; j++) {
            output = output + '*';
         }
         fronSpace++;
         
         console.log(output);
      }
   }

   pattern12() {
      var size: number = 9;
      var jmlStar: number = (size / 2);
      var output: string = '';
      var c: number = (size / 2)-1;

      for (let i = 0; i <= size; i++) {
         output = '';

         if (i < c) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
            jmlStar--;
         } else {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
            jmlStar++;
         }         
         console.log(output);
      }
   }

   pattern13() {
      var size: number = 9;
      var jmlStar: number = (size / 2);
      var frontSpace: number = 0;
      var output: string = '';
      var c: number = (size / 2)-1;

      for (let i = 0; i < size; i++) {
         output = '';

         if (i < c) {
            for (let j = 0; j < frontSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
            jmlStar--;
            frontSpace++;
         } else {
            for (let j = 0; j < frontSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
            jmlStar++;
            frontSpace--;
         }

         console.log(output);
      }
   }

   pattern14() {
      var size: number = 9;
      var jmlStar: number = size / 2;
      var fronSpace: number = 0;
      var c: number = jmlStar - 1;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         output = '';
         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '* ';
            }
            jmlStar--;
            fronSpace++;
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '* ';
            }
            jmlStar++;
            fronSpace--;
         }
         
         console.log(output);
      }
   }

   pattern15() {
      var size: number = 5;
      var jmlStar: number = 0;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         output = '';
         if (i == size) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
         }
         
         jmlStar++;
         console.log(output);
      }
   }

   pattern16() {
      var size: number = 5;
      var jmlStar: number = 0;
      var fronSpace: number = size;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         output = '';
         if (i == size) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
         }
         
         jmlStar++;
         fronSpace--;
         console.log(output);
      }
   }

   pattern17() {
      var size: number = 5;
      var jmlStar: number = size;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         output = '';

         if (i == 0) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
         }
         
         jmlStar--;
         console.log(output);
      }
   }

   pattern18() {
      var size: number = 5;
      var jmlStar: number = size;
      var frontSpace: number = 0;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         output = '';

         if (i == 0) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < frontSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
         }
         
         jmlStar--;
         frontSpace++;
         console.log(output);
      }
   }

   pattern19() {
      var size: number = 5;
      var jmlStar: number = 0;
      var fronSpace: number = size-1;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         output = '';

         if (i == size-1) {
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
         }

         
         jmlStar += 2;
         fronSpace--;
         console.log(output);
      }
   }

   pattern20() {
      var size: number = 5;
      var jmlStar: number = (size * 2) - 1;
      var fronSpace: number = 0;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         output = '';

         if (i == 0) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
         }

         
         jmlStar -= 2;
         fronSpace++;
         console.log(output);
      }
   }

   pattern21() {
      var size: number = 9;
      var jmlStar: number = 0;
      var fronSpace: number = (size / 2) - 1;
      var output: string = '';
      var c: number = fronSpace;

      for (let i = 0; i < size; i++) {
         output = '';

         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
            jmlStar += 2;
            fronSpace--;
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
            jmlStar -= 2;
            fronSpace++;
         }        
         
         console.log(output);
      }
   }

   pattern22() {
      var size: number = 9;
      var jmlStar: number = 0;
      var output: string = '';
      var c: number = (size / 2) - 1;

      for (let i = 0; i < size; i++) {
         output = '';

         if (i < c) {
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
            jmlStar++;
         } else {
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
            jmlStar--;
         }        
         
         console.log(output);
      }
   }

   pattern23() {
      var size: number = 9;
      var jmlStar: number = 0;
      var output: string = '';
      var frontSpace: number = (size / 2) - 1;
      var c: number = frontSpace;

      for (let i = 0; i < size; i++) {
         output = '';

         if (i < c) {
            for (let j = 0; j <= frontSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
            frontSpace--;
            jmlStar++;
         } else {
            for (let j = 0; j <= frontSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
            jmlStar--;
            frontSpace++;
         }        
         
         console.log(output);
      }
   }

   pattern24() {
      var size: number = 5;
      var jmlStar: number = size;
      var output: string = '';
      var fronSpace: number = size;

      for (let i = 0; i < size; i++) {
         output = '';

         if (i == 0 || i == size-1) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
         }

         
         fronSpace--;
         
         console.log(output);
      }
   }

   pattern25() {
      var size: number = 5;
      var jmlStar: number = size;
      var output: string = '';
      var fronSpace: number = 0;

      for (let i = 0; i < size; i++) {
         output = '';

         if (i == 0 || i == size-1) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
         }

         
         fronSpace++;
         
         console.log(output);
      }
   }

   pattern26() {
      var size: number = 9;
      var jmlStar: number = size;
      var star: number = 0;
      var c: number = (size / 2) - 1;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         output = '';

         if (i < c) {
            for (let j = 0; j < star; j++) {
               output = output + '*';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
            for (let j = 0; j < star; j++) {
               output = output + '*';
            }
   
            
            jmlStar -= 2;
            star++;
         } else {
            for (let j = 0; j < star; j++) {
               output = output + '*';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
            for (let j = 0; j < star; j++) {
               output = output + '*';
            }
   
            
            jmlStar += 2;
            star--;
         }

         
         console.log(output);
      }
   }

   pattern27() {
      var size: number = 10;
      var jmlStar: number = 0;
      var fronSpace: number = size / 2;
      var output: string = '';
      let c: number = (size / 2) - 1;

      for (let i = 0; i < size; i++) {
         output = '';

         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + ' ';
            }
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
            jmlStar += 2;
            fronSpace--;
         } else if (i == c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + ' ';
            }
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + ' ';
            }
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
            jmlStar -= 2;
            fronSpace++;
         }
         
         console.log(output);
      }
   }

   pattern28() {
      var size: number = 9;
      var jmlStar: number = 0;
      var fronSpace: number = (size / 2) - 1;
      var output: string = '';
      var c: number = fronSpace;

      for (let i = 0; i < size; i++) {
         output = '';

         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
            jmlStar += 2;
            fronSpace--;
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + ' ';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
            jmlStar -= 2;
            fronSpace++;
         }        
         
         console.log(output);
      }
   }

   pattern29() {
      var size: number = 9;
      var jmlStar: number = 9;
      var fronSpace: number = 1;
      var c: number = (jmlStar/2) - 1;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         output = '';
         if (i == 0 || i == size-1) {
            jmlStar = 9;
            for (let j = 0; j <= (jmlStar/2); j++) {
               output = output + '* ';
            }
         } else {
            if (i < c) {
               for (let j = 0; j < fronSpace; j++) {
                  output = output + ' ';
               }
               for (let j = 0; j < jmlStar-2; j++) {
                  if (j == 0 || j == jmlStar-3) {
                     output = output + '*';
                  } else {
                     output = output + ' ';
                  }
               }
               jmlStar--;
               jmlStar--;
               fronSpace++;
            } else if (i == c) {
               for (let j = 0; j < jmlStar-2; j++) {
                  if (j == 0 || j == jmlStar-3) {
                     output = output + '*';
                  } else {
                     output = output + ' ';
                  }
               }
            } else {
               for (let j = 0; j < fronSpace; j++) {
                  output = output + ' ';
               }
               for (let j = 0; j < jmlStar-2; j++) {
                  if (j == 0 || j == jmlStar-3) {
                     output = output + '*';
                  } else {
                     output = output + ' ';
                  }
               }
               jmlStar++;
               jmlStar++;
               fronSpace--;
            }
         } 
         
         
         console.log(output);
      }
   }

   pattern30() {
      var size: number = 5;
      var jmlStar: number = size;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         output = '';

         for (let j = 0; j <= jmlStar; j++) {
            output = output + '*';
         }
         
         console.log(output);
      }
   }

   pattern31() {
      var size: number = 5;
      var jmlStar: number = size;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         output = '';

         if (i == 0 || i == size-1) {
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + ' ';
               }
            }
         }
         
         console.log(output);
      }
   }

}


// const pattern = new Pattern();
// pattern.pattern27();
const pattern = new Pattern();
pattern.menu(1);